package com.structurizr.model;

import com.structurizr.StructurizrContainerRelations;

public class SourceDependency extends AbstractDependency {

    private final Component targetComponent;
    private final String source;

    public SourceDependency(Component targetComponent, String source) {
        this.targetComponent = targetComponent;
        this.source = source;
    }

    @Override
    public void buildRelation(Container container, StructurizrContainerRelations relations, boolean hierarchicalIdentifiers) {
        relations.addRelation(
                source,
                targetComponent.getPath(hierarchicalIdentifiers),
                getDescription(),
                getTechnology(),
                getTags()
        );
    }
}
