package com.structurizr.model;

import com.structurizr.StructurizrContainerRelations;

public class TargetDependency extends AbstractDependency {

    private final Component sourceComponent;
    private final String target;

    public TargetDependency(Component sourceComponent, String target) {
        this.sourceComponent = sourceComponent;
        this.target = target;
    }

    @Override
    public void buildRelation(Container container, StructurizrContainerRelations relations, boolean hierarchicalIdentifiers) {
        relations.addRelation(
                sourceComponent.getPath(hierarchicalIdentifiers),
                target,
                getDescription(),
                getTechnology(),
                getTags()
        );
    }
}
