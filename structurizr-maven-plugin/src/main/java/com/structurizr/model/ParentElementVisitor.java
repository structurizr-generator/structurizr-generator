package com.structurizr.model;

public class ParentElementVisitor {
    public void visit(ContainingElement parent) {

    }

    public static class ComponentPathVisitor extends ParentElementVisitor {

        private final ElementPath elementPath;

        public ComponentPathVisitor(Component component) {
            this.elementPath = new ElementPath(component);
        }

        @Override
        public void visit(ContainingElement parent) {
            if (parent == null) {
                return;
            }
            parent.fillPath(elementPath);
            parent.accept(this);
        }

        public String getPath(boolean absolute) {
            if (!absolute) {
                return elementPath.first().orElseThrow(IllegalStateException::new);
            }
            return elementPath.reversed();
        }
    }
}
