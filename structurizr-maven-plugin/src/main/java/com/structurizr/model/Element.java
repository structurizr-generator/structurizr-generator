package com.structurizr.model;

import com.structurizr.write.SectionWriter;

import java.io.IOException;

public abstract class Element {

    private final String id;
    private final ContainingElement parent;

    public Element(String id, ContainingElement parent) {
        this.id = id;
        this.parent = parent;
    }

    public String getId() {
        return id;
    }

    public abstract void toDSL(SectionWriter writer) throws IOException;

    public void accept(ElementVisitor elementVisitor) {
        elementVisitor.visit(this);
    }

    public void accept(ParentElementVisitor parentElementVisitor) {
        parentElementVisitor.visit(parent);
    }
}
