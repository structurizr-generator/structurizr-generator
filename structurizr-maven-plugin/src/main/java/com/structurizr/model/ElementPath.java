package com.structurizr.model;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ElementPath {

    private final List<String> pathParts = new LinkedList<>();
    public ElementPath(Element element) {
        pathParts.add(element.getId());
    }

    public void include(Element element) {
        pathParts.add(element.getId());
    }

    public String reversed() {
        return pathParts
                .stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.joining("."));
    }

    public Optional<String> first() {
        return pathParts.stream().findFirst();
    }
}
