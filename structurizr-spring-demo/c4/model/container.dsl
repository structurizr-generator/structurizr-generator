test = container "Test service" "" "" {
   SingleComponent = component "SingleComponent" {
   }

   group "Module1" {
      Component1 = component "Component1" {
      }

      Service1 = component "Service1" {
      }

      group "Repository group" {
         Repository1 = component "Repository1" {
         }

      }

   }

   group "Module2" {
      Service2 = component "Service2" {
         tags Service
      }

      group "infrastructure" {
         RestController2 = component "RestController2" {
         }

      }

      Repository2 = component "Repository2" {
      }

   }

}

Service2 -> Repository2 "" "" 
Service2 -> Service1 "" "" 
Service1 -> Component1 "Calls component" "JVM" "Asynchronous"
Service1 -> Repository1 "" "" 
nginx -> RestController2 "Calls the controller" "HTTPS" "Asynchronous"
Repository2 -> postgresql "Reads and writes to" "" 
RestController2 -> Service2 "" "" 
Repository1 -> postgresql "Reads and writes to" "" 
