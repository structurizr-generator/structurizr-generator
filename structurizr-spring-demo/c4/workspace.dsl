workspace {

    model {
        properties {
            "structurizr.groupSeparator" "/"
        }

        user = person "User"

        springDemo = softwareSystem "Spring Demo" {
            !include infrastructure

            postgresql = container "Database" "PostgreSQL" {
                tags Database
            }

            !include model/container.dsl

        }

        user -> nginx "Calls"
    }

    views {
        styles {
            !include styles
        }
    }

}