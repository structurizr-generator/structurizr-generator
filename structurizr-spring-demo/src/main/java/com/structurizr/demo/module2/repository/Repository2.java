package com.structurizr.demo.module2.repository;

import com.structurizr.annotations.Uses;
import com.structurizr.demo.module2.model.Entity2;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;

@Uses(value = "postgresql", description = "Reads and writes to")
@Repository
public interface Repository2 extends R2dbcRepository<Entity2, Long> {
}
